using System;
using UnityEngine;
using System.Collections;
//using Random = UnityEngine.Random;

namespace UnityStandardAssets.Effects
{
    public class ParticleSystemMultiplier : MonoBehaviour
    {
        // a simple script to scale the size, speed and lifetime of a particle system

        public float multiplier = 1;

        Quaternion rotationSelected;

        private void Start()
        {
            InvokeRepeating("ChangeSizeFire", 1F, 1F);                     // Alejo's Modification
            InvokeRepeating("ChangeAngleFire", 1F, 4F);                     // Alejo's Modification
            var systems = GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem system in systems)
            {
                system.startSize *= multiplier;
                system.startSpeed *= multiplier;
                system.startLifetime *= Mathf.Lerp(multiplier, 1, 0.5f);
                system.Clear();
                system.Play();
            }
        }

        void ChangeSizeFire()                                              // Alejo's Modification  ****************
        {
            multiplier = UnityEngine.Random.Range(1F, 2F);
            var systems = GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem system in systems)
            {
                if(system.name == "Flames")
                {
                    system.startSize = multiplier;
                }               
                system.Play();
            }
        }
        
        void ChangeAngleFire()
        {
            var systems = GetComponentsInChildren<ParticleSystem>();

            foreach (ParticleSystem system in systems)
            {
                if (system.name == "SmokeDark")
                {
                    system.transform.rotation = Quaternion.Euler(UnityEngine.Random.Range(200, 300), 0, 0);
                    rotationSelected = system.transform.rotation;
                }
                if(system.name == "SmokeLit")
                {
                    system.transform.rotation = rotationSelected;
                }
                system.Play();
            }
        }                                                                   // ************************
    }
}
