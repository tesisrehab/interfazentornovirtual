﻿using UnityEngine;
using System.Collections;
using Mono.Data.Sqlite;
using System;
using System.Data; 

public class Database : MonoBehaviour {

	IDbConnection dbconn;
	IDbCommand dbcmd;
	IDataReader reader;
	int idPaciente;
	int idSesion;

	void Connection(){

        string conection = "Data Source=C:\\Database\\DBPacientes.db"; // Ubicación base de datos

        dbconn = (IDbConnection) new SqliteConnection(conection);
		dbconn.Open(); //Abre la conexión a la base de datos
	}

	void SetIdPaciente(){
		dbcmd = dbconn.CreateCommand();
		string sqlQuery = "SELECT idPaciente FROM sesion_actual";
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();
		var idPaciente1 = 0;
		while (reader.Read())
		{
			idPaciente1 = reader.GetInt32 (0);
		}
		reader.Close();

		idPaciente = idPaciente1;
	}

	Patient GetPaciente(int idPaciente){
		string Query = "SELECT * FROM Pacientes WHERE idPaciente = " + idPaciente.ToString() + ";";
		dbcmd = dbconn.CreateCommand();
		dbcmd.CommandText = Query;
		reader = dbcmd.ExecuteReader();

		Patient paciente = new Patient();

		while (reader.Read())
		{
			paciente.FullName = reader.GetString (2);
			paciente.DocType = reader.GetString (3);
			paciente.IdNumber = reader.GetString (4);
			paciente.RegDate = reader.GetDateTime (5);
			paciente.Age = reader.GetInt16 (6);
			paciente.Sex = reader.GetString (7);
			paciente.CivState = reader.GetString (8);

			break;
		}

		reader.Close ();
		return paciente;
	}

	void StartSession(){
		string actual = DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss");
		SetIdPaciente ();
		if (idPaciente > 0) {
			try{
				string insertSession = String.Format("INSERT INTO Sesion(idPaciente, Fecha) VALUES ({0}, '{1}');", idPaciente, actual);
				dbcmd = dbconn.CreateCommand();
				dbcmd.CommandType = CommandType.Text;
				dbcmd.CommandText = insertSession;

				reader = dbcmd.ExecuteReader();
				reader.Close();
				dbconn.Close();

			}catch(Exception e){
				Debug.Log ("Error al guardar en la base de datos: " + e);
			}
		}
	}

	void GetSession(){
		dbconn.Open ();
		dbcmd = dbconn.CreateCommand();
		string sqlQuery = String.Format("SELECT idSesion FROM Sesion WHERE idPaciente={0} ORDER BY idSesion DESC LIMIT 1;", idPaciente);
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();

		while (reader.Read())
		{
			idSesion = reader.GetInt32 (0);
		}
		reader.Close();
	}
/*
	void SaveLevel(Level nivel){
		if (idSesion > 0 && nivel != null) {
			try{
				string saveLevel = String.Format ("INSERT INTO Nivel (idSesion, tiempo, intentos, error, premios) VALUES ({0}, {1}, {2}, {3}, {4})", idSesion, nivel.tiempo, nivel.intentos, nivel.error, nivel.premios);
				dbcmd = dbconn.CreateCommand ();
				dbcmd.CommandText = saveLevel;
				Debug.Log(saveLevel);
				reader = dbcmd.ExecuteReader ();
				dbconn.Close();
			}catch(Exception e){
				Debug.Log (e);
			}

		}
	}

	// Use this for initialization
	void Start () {
		Connection ();
		StartSession ();
		GetSession ();

		Level nivel = new Level ();
		nivel.tiempo = 1.4f;
		nivel.intentos = 2;
		nivel.error = 0.5f;
		nivel.premios = 2;

		SaveLevel (nivel);
	}*/
	
	// Update is called once per frame
	void Update () {
	
	}
}
