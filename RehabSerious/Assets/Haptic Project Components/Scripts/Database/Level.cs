﻿using System;
using System.Text;

public class Level{

	public float AvgTime { get; set; }
	public float TimeOfGame { get; set; }
    public string LevelPlayed { get; set; }
    public int ObjectsHit { get; set; }	
    public int ObjectsNotHit { get; set; }
    public int GoodPoints { get; set; }
    public int MediumPoints { get; set; }
    public int BadPoints { get; set; }
    public int Awards { get; set; }

    public Level (){
		
	}
}


