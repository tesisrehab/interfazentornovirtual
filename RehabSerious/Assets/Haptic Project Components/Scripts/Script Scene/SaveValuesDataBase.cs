﻿using UnityEngine;
using Mono.Data.Sqlite;
using System;
using System.Data;

public class SaveValuesDataBase : MonoBehaviour {  // Milton's Code


    private static float averageTime;           // Average time that takes kill one objective
    private static string levelPlayed;          // Level played by the user
    private static float timeOfGame;            // Time that the user played, on minutes
    private static int numberObjectsHitted;     // Number of objects that user destroyed
    private static int numberObjectsNoHitted;   // Number of objects no destroyed by the user
    private static int numberGoodPoints;        // Good, medium and bad points ---
    private static int numberMediumPoints;
    private static int numberBadPoints;         // ----
    private static int awards;
    private bool flag1 = false;

    // Database Connection
    IDbConnection dbconn;
    IDbCommand dbcmd;
    IDataReader reader;
    int idPaciente;
    int idSesion;

    // Use this for initialization
    void Start () {
        Connection();
        StartSession();
        GetSession();
    }

    void Awake() {
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Reset.isReset && !flag1)
        {
            //Debug.Log("ROMERO ES GAY");
            flag1 = true;
            SaveValuesOnDataBase();
        }
	}

    void Connection()
    {
        string conection = "Data Source=C:\\Database\\DBPacientes.db"; // Ubicación base de datos
        dbconn = (IDbConnection)new SqliteConnection(conection);
        dbconn.Open(); //Abre la conexión a la base de datos
    }

    void SetIdPaciente()
    {
        dbcmd = dbconn.CreateCommand();
        string sqlQuery = "SELECT idPaciente FROM sesion_actual";
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();
        var idPaciente1 = 0;
        while (reader.Read())
        {
            idPaciente1 = reader.GetInt32(0);
        }
        reader.Close();

        idPaciente = idPaciente1;
    }

    Patient GetPaciente(int idPaciente)
    {
        string Query = "SELECT * FROM Pacientes WHERE idPaciente = " + idPaciente.ToString() + ";";
        dbcmd = dbconn.CreateCommand();
        dbcmd.CommandText = Query;
        reader = dbcmd.ExecuteReader();

        Patient paciente = new Patient();

        while (reader.Read())
        {
            paciente.FullName = reader.GetString(2);
            paciente.DocType = reader.GetString(3);
            paciente.IdNumber = reader.GetString(4);
            paciente.RegDate = reader.GetDateTime(5);
            paciente.Age = reader.GetInt16(6);
            paciente.Sex = reader.GetString(7);
            paciente.CivState = reader.GetString(8);

            break;
        }

        reader.Close();
        return paciente;
    }

    void StartSession()
    {
        string actual = DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss");
        //Debug.Log(actual);
        SetIdPaciente();
        if (idPaciente > 0)
        {
            try
            {
                string insertSession = String.Format("INSERT INTO Sesion(idPaciente, Fecha) VALUES ({0}, '{1}');", idPaciente, actual);
                dbcmd = dbconn.CreateCommand();
                dbcmd.CommandType = CommandType.Text;
                dbcmd.CommandText = insertSession;

                reader = dbcmd.ExecuteReader();
                reader.Close();
                dbconn.Close();

            }
            catch (Exception e)
            {
                Debug.Log("Error al guardar en la base de datos: " + e);
            }
        }
    }

    void GetSession()
    {
        dbconn.Open();
        dbcmd = dbconn.CreateCommand();
        string sqlQuery = String.Format("SELECT idSesion FROM Sesion WHERE idPaciente={0} ORDER BY idSesion DESC LIMIT 1;", idPaciente);
        dbcmd.CommandText = sqlQuery;
        reader = dbcmd.ExecuteReader();

        while (reader.Read())
        {
            idSesion = reader.GetInt32(0);
        }
        reader.Close();
    }

    void SaveLevel(Level nivel)
    {
        if (idSesion > 0 && nivel != null)
        {
            try
            {
                string saveLevel = String.Format("INSERT INTO Nivel (idSesion, AvgTime, TimeOfGame, LevelPlayed, ObjectsHit, ObjectsNotHit, GoodPoints, MediumPoints, BadPoints, Awards) VALUES ({0}, {1:0.00}, {2:0.00}, \"{3}\", {4}, {5}, {6}, {7}, {8}, {9})", 
                    idSesion, nivel.AvgTime, nivel.TimeOfGame, levelPlayed, nivel.ObjectsHit, nivel.ObjectsNotHit, nivel.GoodPoints, nivel.MediumPoints, nivel.BadPoints, nivel.Awards);
                Debug.Log(saveLevel);
                dbcmd = dbconn.CreateCommand();
                dbcmd.CommandText = saveLevel;
                reader = dbcmd.ExecuteReader();
                dbconn.Close();
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

        }
    }

    public void SaveValuesOnDataBase()
    {
        levelPlayed = SelectFeaturesByLevel.level;   // I consider that these are all the values that must be save on database

        timeOfGame = ValuesMainMenuScene.minutesGame;

        averageTime = PointsCounter.GetAverageOfTimes();
        numberGoodPoints = PointsCounter.goodPoints;
        numberMediumPoints = PointsCounter.mediumPoints;
        numberBadPoints = PointsCounter.badPoints;

        numberObjectsNoHitted = PointsCounter.GetNotHittedObjectives();
        numberObjectsHitted = numberBadPoints + numberGoodPoints + numberMediumPoints;
        awards = PointsCounter.awards;

        Debug.Log("Tiempo: " + averageTime);
        Level nivel = new Level();

        nivel.AvgTime = averageTime;
        nivel.TimeOfGame = timeOfGame;
        nivel.ObjectsHit = numberObjectsHitted;
        nivel.ObjectsNotHit = numberObjectsNoHitted;
        nivel.GoodPoints = numberGoodPoints;
        nivel.MediumPoints = numberMediumPoints;
        nivel.BadPoints = numberBadPoints;
        nivel.Awards = awards;
        nivel.LevelPlayed = levelPlayed;
       
        SaveLevel(nivel);
    }
}
