﻿using UnityEngine;
using System.Collections;

public class SelectFeaturesByLevel : MonoBehaviour {

    public static string  level = "easy";

    public static bool    forcesActivated = false;
    public static float   magnitudeForceSelected = 0.0F;
    public static float   gainForceSelected = 0.0F;
    public static float   frecuenceForceSelected = 0.0F;
    public static float[] directionForceSelected = new float[3];

    public static bool    vibrationActivated = false;
    public static float   magnitudeVibrationSelected = 0.0F;
    public static float   gainVibrationSelected = 0.0F;
    public static float   frecuenceVibrationSelected = 0.0F;

    public static bool    viscosityActivated = false;
    public static float   magnitudeViscositySelected = 0.0F;
    public static float   gainViscositySelected = 0.0F;

    public static bool    changeAxisAutomatic = false;                         // Variable only for personalized level

    public static GenericFunctionsClass myGenericFunctionsClassScriptT;
    
    void Awake() {
        myGenericFunctionsClassScriptT = transform.GetComponent<GenericFunctionsClass>();  // Object that will update the Omni
    }

    /* Use this for initialization */
    void Start () {
        InvokeRepeating("ChangeAxisForces", 1, 3.75F);          // Remember that this function update forces and viscosity
        InvokeRepeating("ChangeMagnitudeVibration", 1, 2.75F);  // Remember that this function update vibration
        ReadValuesFromPreviousScene();                          // Read values from MenuScene
    }
	
	/* Update is called once per frame */
	void Update () {

	}


    void ReadValuesFromPreviousScene()
    {
        if (ValuesMainMenuScene.isADefaultLevel)  // If the user got a Default level, then...
        {
            ChangeLevel(ValuesMainMenuScene.levelChoosen);
        }
        else  // ... else, we're going to set all the choosen variables
        {
            level = "personalized";

            if (ValuesMainMenuScene.constantForceChoosen != 0)  // Change value of Constant Force
            {
                forcesActivated = true;
                magnitudeForceSelected = ValuesMainMenuScene.constantForceChoosen;
                gainForceSelected = ValuesMainMenuScene.constantForceChoosen;
                frecuenceForceSelected = 0.0F;

                directionForceSelected[0] = 0F;
                directionForceSelected[1] = -1F;
                directionForceSelected[2] = 0F;
            }
            else
            {
                forcesActivated = false;
            }


            if (ValuesMainMenuScene.viscosityChoosen != 0)      // Change Viscosity
            {
                viscosityActivated = true;
                gainViscositySelected = ValuesMainMenuScene.viscosityChoosen;
                magnitudeViscositySelected = ValuesMainMenuScene.viscosityChoosen;
            }
            else
            {
                viscosityActivated = false;
            }


            if (ValuesMainMenuScene.vibrationChoosen != 0)      // Change vibration
            {
                vibrationActivated = true;
                magnitudeVibrationSelected = ValuesMainMenuScene.vibrationChoosen;
                gainVibrationSelected = ValuesMainMenuScene.vibrationChoosen;
                frecuenceVibrationSelected = 20F;
            }
            else
            {
                vibrationActivated = false;
            }


            if (ValuesMainMenuScene.isRandomAxis)               // Change if user wants switch of axis forces
            {
                changeAxisAutomatic = true;
            }
            else
            {
                changeAxisAutomatic = false;
            }


            ChangeScene();                                      // Change of scene's level.                            


            GenerateObjective.velocityAppear = ValuesMainMenuScene.velocityAppear;        // Change of Appear Velocity
            DeleteObjective.velocityDisappear = ValuesMainMenuScene.velocityDisappear;    // Change of Disappear velocity
        }
    }

    public static void ChangeLevel(string newLevel)   // If, User wants a Default level ....  
    {
        if((newLevel == "easy" || newLevel == "medium" || newLevel == "hard" || newLevel == "veryHard" || newLevel == "legend"))
        {
            level = newLevel;

            ChangeExternallyForces();
            ChangeScene();
            ChangeVelocity();
        }
    }

    static void ChangeScene()
    {
        GameObject scene = GameObject.Find("Scene");

        if (level == "easy" || level == "medium" || (level == "personalized" && ValuesMainMenuScene.lowerSceneLevel))
        {
            scene.transform.position = new Vector3(0.82F,-1.16F,5.58F);
        }
        else
        {
            scene.transform.position = new Vector3(0.82F, 0.21F, 5.58F);
        }
    }

    static void ChangeVelocity()
    {
        if(level == "easy" || level == "medium")
        {
            GenerateObjective.velocityAppear  = 3F;
            DeleteObjective.velocityDisappear = 6.5F;
        }
        else
        {
            GenerateObjective.velocityAppear  = 2F;
            DeleteObjective.velocityDisappear = 5F;
        }        
    }

    static void ChangeExternallyForces()
    {
        if(level == "easy")   // "easy" levels is for dummies man!, in other words, that level doesn't have environment haptic
        {                     // features
            forcesActivated = false;
            vibrationActivated = false;
            viscosityActivated = false;
        }

        if(level == "medium")
        {
            forcesActivated = true;              //// Forces
            
            directionForceSelected[0] = 0F;   //  Axis x
            directionForceSelected[1] = -1F;  //  Axis y
            directionForceSelected[2] = 0F;   //  Axis z

            gainForceSelected = 0.2F;
            magnitudeForceSelected = 0.2F;
            frecuenceForceSelected = 0F;

            vibrationActivated = false;          //// Vibration
             
            viscosityActivated = true;           //// Viscosity

            magnitudeViscositySelected = 0.4F;
            gainViscositySelected = 0.4F;
        }

        if (level == "hard")
        {
            forcesActivated = true;              //// Forces

            directionForceSelected[0] = 0F;
            directionForceSelected[1] = 1F;
            directionForceSelected[2] = 0F;

            gainForceSelected = 0.5F;
            magnitudeForceSelected = 0.5F;
            frecuenceForceSelected = 0F;
            
            vibrationActivated = false;          //// Vibration

            viscosityActivated = true;           //// Viscosity

            magnitudeViscositySelected = 0.6F;
            gainViscositySelected = 0.6F;
        }

        if (level == "veryHard")
        {
            forcesActivated = true;                //// Forces

            directionForceSelected[0] = 0F;
            directionForceSelected[1] = 1F;
            directionForceSelected[2] = 0F;

            gainForceSelected = 0.8F;
            magnitudeForceSelected = 0.75F;
            frecuenceForceSelected = 0F;

            vibrationActivated = true;            //// Vibration

            gainVibrationSelected = 0.5F;
            magnitudeVibrationSelected = 0.5F;
            frecuenceVibrationSelected = 20;

            viscosityActivated = true;            //// Viscosity

            magnitudeViscositySelected = 0.8F;
            gainViscositySelected = 0.8F;
        }
        
        if (level == "legend")  // Is not for dummies, please, if you're a dumb don't choose this option
        {
            forcesActivated = true;               //// Forces

            directionForceSelected[0] = 1F;
            directionForceSelected[1] = 1F;
            directionForceSelected[2] = 0F;

            gainForceSelected      = 1F;
            magnitudeForceSelected = 1F;
            frecuenceForceSelected = 0F;

            vibrationActivated = true;            //// Vibration

            gainVibrationSelected = 1F;
            magnitudeVibrationSelected = 1F;
            frecuenceVibrationSelected = 40;

            viscosityActivated = true;            //// Viscosity

            magnitudeViscositySelected = 10F;
            gainViscositySelected = 10F;
        }
    }


/*   ------- Invoke's Functions -------   */
void ChangeAxisForces()
    {
        if(level == "legend" || (level == "personalized" && changeAxisAutomatic))  // Change Axis Forces if user is on "legend" 
        {                                                                          // level or if he's on "personalized" level and
            int index = (int)Random.Range(1, 6);                                   // chose randomly axis
            switch (index)
            {
                case 1:
                    directionForceSelected[0] = -1F;
                    directionForceSelected[1] = 0F;
                    directionForceSelected[2] = 0F;
                    break;
                case 2:
                    directionForceSelected[0] = 1F;
                    directionForceSelected[1] = 0F;
                    directionForceSelected[2] = 0F;
                    break;
                case 3:
                    directionForceSelected[0] = 0F;
                    directionForceSelected[1] = -1F;
                    directionForceSelected[2] = 0F;
                    break;
                case 4:
                    directionForceSelected[0] = 0F;
                    directionForceSelected[1] = 1F;
                    directionForceSelected[2] = 0F;
                    break;
                case 5:
                    directionForceSelected[0] = 0F;
                    directionForceSelected[1] = 0F;
                    directionForceSelected[2] = -1F;
                    break;
                case 6:
                    directionForceSelected[0] = 0F;
                    directionForceSelected[1] = 0F;
                    directionForceSelected[2] = 1F;
                    break;
            }
        }

        myGenericFunctionsClassScriptT.SetEnvironmentConstantForce();
        myGenericFunctionsClassScriptT.SetEnvironmentViscosity();  // We take advantage of this method for update the viscosity
    }

    void ChangeMagnitudeVibration()
    {
        if(level == "veryHard")  // All that values were a personal decision
        {
            int index = (int)Random.Range(1, 6);
            switch (index)
            {
                case 1:
                    gainVibrationSelected = 1F;
                    magnitudeVibrationSelected = 1F;
                    frecuenceVibrationSelected = 40;
                    break;
                case 2:
                    gainVibrationSelected = 0.95F;
                    magnitudeVibrationSelected = 0.9F;
                    frecuenceVibrationSelected = 30;
                    break;
                case 3:
                    gainVibrationSelected = 0.8F;
                    magnitudeVibrationSelected = 0.9F;
                    frecuenceVibrationSelected = 50;
                    break;
                case 4:
                    gainVibrationSelected = 0.88F;
                    magnitudeVibrationSelected = 0.94F;
                    frecuenceVibrationSelected = 20;
                    break;
                case 5:
                    gainVibrationSelected = 0.92F;
                    magnitudeVibrationSelected = 0.96F;
                    frecuenceVibrationSelected = 35;
                    break;
                case 6:
                    gainVibrationSelected = 0.9F;
                    magnitudeVibrationSelected = 0.9F;
                    frecuenceVibrationSelected = 25;
                    break;
            }
        }

        if (level == "legend")
        {
            int index = (int)Random.Range(1, 6);
            switch (index)
            {
                case 1:
                    gainVibrationSelected = 0.5F;
                    magnitudeVibrationSelected = 0.5F;
                    frecuenceVibrationSelected = 20;
                    break;
                case 2:
                    gainVibrationSelected = 0.42F;
                    magnitudeVibrationSelected = 0.3F;
                    frecuenceVibrationSelected = 60;
                    break;
                case 3:
                    gainVibrationSelected = 0.2F;
                    magnitudeVibrationSelected = 0.2F;
                    frecuenceVibrationSelected = 50;
                    break;
                case 4:
                    gainVibrationSelected = 0.65F;
                    magnitudeVibrationSelected = 0.7F;
                    frecuenceVibrationSelected = 15;
                    break;
                case 5:
                    gainVibrationSelected = 0.32F;
                    magnitudeVibrationSelected = 0.45F;
                    frecuenceVibrationSelected = 10;
                    break;
                case 6:
                    gainVibrationSelected = 0.44F;
                    magnitudeVibrationSelected = 0.55F;
                    frecuenceVibrationSelected = 30;
                    break;
            }
        }
        
        myGenericFunctionsClassScriptT.SetVibrationMotor();
    }
    /*   ------- Invoke's Functions -------   */
}