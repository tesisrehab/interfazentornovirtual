﻿using UnityEngine;
using System.Collections;

public class Reset : MonoBehaviour {

    public static bool isReset = false;

    public Canvas resetCanvas;
    
    public UnityEngine.UI.Image star1;
    public UnityEngine.UI.Image star2;
    public UnityEngine.UI.Image star3;

    private string calification;

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isReset)
        {
            GetActionsReset();
        }
        else
        {
            GetActionsUnreset();
        }
	}

    void GetActionsReset()
    {
        resetCanvas.enabled = true;

        star1.enabled = false;
        star2.enabled = false;
        star3.enabled = false;

        if(PointsCounter.notHittedObjectves == PointsCounter.totalObjectives || PointsCounter.notHittedObjectves == (PointsCounter.totalObjectives - 1)) 
        {
            PointsCounter.awards = 0;
            calification = "veryBad";
        }
        else if(PointsCounter.goodPoints >= PointsCounter.mediumPoints && PointsCounter.goodPoints > PointsCounter.badPoints)
        {
            PointsCounter.awards = 3;
            calification = "good";
        }
        else if(PointsCounter.mediumPoints >= PointsCounter.goodPoints && PointsCounter.mediumPoints > PointsCounter.badPoints)
        {
            PointsCounter.awards = 2;
            calification = "medium";
        }
        else if(PointsCounter.badPoints > PointsCounter.goodPoints && PointsCounter.badPoints >= PointsCounter.mediumPoints)
        {
            PointsCounter.awards = 1;
            calification = "bad";
        }
        else
        {
            PointsCounter.awards = 1;
            calification = "bad";
        }

        if(calification == "veryBad")
        {
            star1.enabled = false;
            star2.enabled = false;
            star3.enabled = false;
        }
        else if(calification == "good")
        {
            star1.enabled = true;
            star2.enabled = true;
            star3.enabled = true;
        }
        else if(calification == "medium")
        {
            star1.enabled = true;
            star2.enabled = false;
            star3.enabled = true;
        }
        else if (calification == "bad")
        {
            star1.enabled = false;
            star2.enabled = true;
            star3.enabled = false;
        }

        if (Pause.isPaused)
        {
            Pause.isPaused = false;
        }

        Time.timeScale = 0;

        print(PointsCounter.GetAverageOfTimes());
        SaveValuesDataBase save = new SaveValuesDataBase();
        save.SaveValuesOnDataBase();
        //SaveValuesDataBase.SaveValuesOnDataBase();  //  Milton's code
    }

    void GetActionsUnreset()
    {
        resetCanvas.enabled = false;
    }
}
