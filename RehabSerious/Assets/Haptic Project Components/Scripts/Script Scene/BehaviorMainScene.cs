﻿using UnityEngine;
using System.Collections;

public class BehaviorMainScene : MonoBehaviour {

    private GameObject Hammer;

    private int counterFramesForButtons = 0;
    private int counterFramesForSounds = 0;
    private int counterClock = 0;
    private float minutesElapsed = 0;
    private int secondsElapsed = 0;

    public static bool flag = false;   // That variable makes that the user must to hold off the button for press again if he wants
                                       // unpause the game

    public UnityEngine.UI.Text timeText;
    public UnityEngine.UI.Text goodText;
    public UnityEngine.UI.Text mediumText;
    public UnityEngine.UI.Text badText;

    public AudioSource frog1;
    public AudioSource frog2;

    public AudioSource wind;

    public AudioSource pauseAudio;
    public AudioSource UnpauseAudio;
    public AudioSource resetAudio;


    // Use this for initialization
    void Start() {
        Pause.isPaused = true;
        Hammer = GameObject.Find("Hammer");

        minutesElapsed = ValuesMainMenuScene.minutesGame;
        InvokeRepeating("CalculateClock",0.1F,1F);

        frog1.volume = 0.1F;
        frog2.volume = 0.1F;
        wind.Play();
    }
	
	// Update is called once per frame
	void Update () {
        counterFramesForButtons++;   // It's a counter that defines when read the buttons (The reading is each 10 frames)
        counterFramesForSounds++;

        UpdateTextScore();
        GetValuesButtons();
        GetSoundsEffects();
    }

    void CalculateClock()
    {
        counterClock++;

        if(secondsElapsed != 0)
        {
            secondsElapsed--;
        }
        else
        {
            if(minutesElapsed != 0)
            {
                secondsElapsed = 59;
                minutesElapsed--;
            }
            else
            {
                resetAudio.Play();
                CancelInvoke("CalculateClock");
                Reset.isReset = true;              
            }
        }

        if (minutesElapsed < 10)
        {
            timeText.text = "0";
            timeText.text += minutesElapsed.ToString();
        }
        else
        {
            timeText.text = minutesElapsed.ToString();
        }

        timeText.text += ":";
        if (secondsElapsed < 10)
        {
            timeText.text += "0";
        }
                
        timeText.text += secondsElapsed.ToString();
    }
    
    void GetValuesButtons()
    {
        if (PluginImport.GetButton1State() && Pause.isPaused && counterFramesForButtons >= 10 && !flag)
        {
            UnpauseAudio.Play();
            Pause.isPaused = false;
            counterFramesForButtons = 0;
            flag = true;
        }
        else if (PluginImport.GetButton1State() && !Pause.isPaused && counterFramesForButtons >= 10 && !flag)
        {
            pauseAudio.Play();
            Pause.isPaused = true;
            counterFramesForButtons = 0;
            flag = true;
        }
        else if (!PluginImport.GetButton1State())
        {
            flag = false;
        }
    }

    void GetSoundsEffects()
    {
        if (counterFramesForSounds >= 300)
        {
            switch (Random.Range(0, 500) % 10)
            {
                case 0:
                    frog1.Play();
                    break;
                case 1:
                    frog2.Play();
                    break;
                case 2:
                    frog1.Stop();
                    break;
                case 3:
                    frog2.Stop();
                    break;
                case 4:
                    frog1.Pause();
                    break;
                case 5:
                    frog2.Pause();
                    break;
                case 6:
                    frog1.UnPause();
                    break;
                case 7:
                    frog2.UnPause();
                    break;
                default:
                    break;
            }

            counterFramesForSounds = 0;
        }
    }

    void UpdateTextScore()
    {
        goodText.text = "Puntos Buenos =";
        goodText.text += PointsCounter.goodPoints.ToString();

        mediumText.text = "Puntos Medios =";
        mediumText.text += PointsCounter.mediumPoints.ToString();

        badText.text = "Puntos Malos =";
        badText.text += PointsCounter.badPoints.ToString();
    }

    public void ExitButtonPressed()
    {
        Application.Quit();
    }

    public void ReturnMainMenuPressed()
    {
        if (Pause.isPaused)
        {
            Pause.isPaused = false;
        }

        if (Reset.isReset)
        {
            Reset.isReset = false;
        }
        
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
}
