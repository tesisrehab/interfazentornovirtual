﻿using UnityEngine;
using System.Collections;

public class PrintButtonHaptic : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("Print", 0, 1);
	}
	
	// Update is called once per frame
	void Update ()
    {
        print(PluginImport.GetButton1State());
    }

    void Print()
    {
        print(PluginImport.GetButton1State());
    }
}
