﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

    public static bool isPaused = false;

    public Canvas pauseMenu;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (isPaused)
        {
            if(!Reset.isReset)  GetActionsPaused();
        }
        else
        {
            GetActionsUnpaused();
        }
    }

    public void GetActionsPaused()
    {
        Time.timeScale = 0;
        pauseMenu.enabled = true;
    }
    
    public void GetActionsUnpaused()
    {
        Invoke("MakeActions", 0.05F);  // This invoke makes that disappearing menu delays (it's more beautiful)
        Time.timeScale = 1;
    }

    void MakeActions()
    {
        pauseMenu.enabled = false;
    }
}
