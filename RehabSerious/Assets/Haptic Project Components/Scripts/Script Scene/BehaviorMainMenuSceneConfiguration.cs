﻿using UnityEngine;
using System.Collections;

public class BehaviorMainMenuSceneConfiguration : MonoBehaviour {

    public UnityEngine.UI.Toggle togglePerform;

    public UnityEngine.UI.Dropdown listOfLevels;

    public UnityEngine.UI.Slider vibrationSlider;
    public UnityEngine.UI.Slider viscositySlider;
    public UnityEngine.UI.Slider constantForceSlider;

    public UnityEngine.UI.Toggle changeAxisAutomaticallyToogle;
    public UnityEngine.UI.Toggle upperSceneLevelToggle;
    public UnityEngine.UI.Toggle lowerSceneLevelToggle;

    public UnityEngine.UI.Slider appearVelocitySlider;
    public UnityEngine.UI.Slider disappearVelocitySlider;

    public UnityEngine.UI.Slider minutesSlider;
    public UnityEngine.UI.Text   minutesText;

    public UnityEngine.UI.Text vibrationText;
    public UnityEngine.UI.Text viscosityText;
    public UnityEngine.UI.Text constantForceText;
    public UnityEngine.UI.Text appearVelocityText;
    public UnityEngine.UI.Text disappearVelocityText;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if(togglePerform.isOn == true)  // Makes that a part of menu be no interactable
        {
            listOfLevels.interactable = false;
            vibrationSlider.interactable = true;
            viscositySlider.interactable = true;
            constantForceSlider.interactable = true;
            changeAxisAutomaticallyToogle.interactable = true;
            upperSceneLevelToggle.interactable = true;
            lowerSceneLevelToggle.interactable = true;
            appearVelocitySlider.interactable = true;
            disappearVelocitySlider.interactable = true;
        }
        else
        {
            listOfLevels.interactable = true;
            vibrationSlider.interactable = false;
            viscositySlider.interactable = false;
            constantForceSlider.interactable = false;
            changeAxisAutomaticallyToogle.interactable = false;
            upperSceneLevelToggle.interactable = false;
            lowerSceneLevelToggle.interactable = false;
            appearVelocitySlider.interactable = false;
            disappearVelocitySlider.interactable = false;
        }

        if(constantForceSlider.value == 0)  // If there is not force then there is not change of axis force
        {
            changeAxisAutomaticallyToogle.isOn = false;
            changeAxisAutomaticallyToogle.interactable = false;
        }
        else
        {
            changeAxisAutomaticallyToogle.interactable = true;
        }

        UpdateTextbox();
	}

    public void BehaviorUpperLevelToogle()  // That subrutine makes that the option of two levels switch between only two options
    {                                       // it is invoke when you click UpperLevelToggle (Method OnClick(), check it into Dummy GameObject)
        if (lowerSceneLevelToggle.isOn && upperSceneLevelToggle.isOn)
        {
            lowerSceneLevelToggle.isOn = false;
        }
        else if(!lowerSceneLevelToggle.isOn && !upperSceneLevelToggle.isOn)
        {
            lowerSceneLevelToggle.isOn = true;
        }
    }

    public void BehaviorLowerLevelToogle()  // That subrutine makes that the option of two levels switch between only two options
    {                                       // it is invoke when you click LowerLevelToggle (Method OnClick(), check it into Dummy GameObject)
        if (lowerSceneLevelToggle.isOn && upperSceneLevelToggle.isOn)
        {
            upperSceneLevelToggle.isOn = false;
        }
        else if(!lowerSceneLevelToggle.isOn && !upperSceneLevelToggle.isOn)
        {
            upperSceneLevelToggle.isOn = true;
        }
    }

    void UpdateTextbox()
    {
        minutesText.text = minutesSlider.value.ToString();
        vibrationText.text = vibrationSlider.value.ToString("F2");
        viscosityText.text = viscositySlider.value.ToString("F2");
        constantForceText.text = constantForceSlider.value.ToString("F2");
        appearVelocityText.text = appearVelocitySlider.value.ToString("F2");
        disappearVelocityText.text = disappearVelocitySlider.value.ToString("F2");
    }
}
