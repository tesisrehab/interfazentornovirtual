﻿using UnityEngine;
using System.Collections;

public class InvokesFading : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("CallFading", 5.8F, 10F);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown)
        {
            CancelInvoke("CallFading");
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
	}

    private void CallFading()
    {
        WaitAMoment();
        CancelInvoke("CallFading");
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    IEnumerator WaitAMoment()
    {
        float fadeTime = GameObject.Find("Dummy").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);  
    }
}
