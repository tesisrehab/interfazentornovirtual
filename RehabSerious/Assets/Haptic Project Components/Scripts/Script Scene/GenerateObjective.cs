﻿using UnityEngine;
using System.Collections;

public class GenerateObjective : MonoBehaviour {

    public GameObject frog1;   // GameObjects that will be instantiate
    public GameObject frog2;
    public GameObject frog3;
    public GameObject shark;
    public GameObject elephant;
    private GameObject Objective;

    public static float velocityAppear = 3F;

    private Quaternion rotation1;
    private Quaternion rotation2;
    private Quaternion rotation3;
    
    Vector3 newPosition = new Vector3(0, 0, 0);

    void Awake()
    {
        velocityAppear = ValuesMainMenuScene.velocityAppear;  // Update the variable with the saved in before scene
    }

    /* Use this for initialization */
    void Start () {
        InvokeRepeating("ChangePosition", 1F, velocityAppear);  // If Pause Mode is on, the reference time is 0, for this reason
    }                                                           // all of InvokeRepeating set in stop
	
	/* Update is called once per frame */
	void Update () {

	}


    void ChangePosition()
    {
        int index = Random.Range(0,100);
        index = index % 11;

        if (index == 0 || index == 1 || index == 2)
        {
            Objective = frog1;
            rotation1 = Quaternion.Euler(-59.14F, 184.61F, 87.2F); //upper matrix
            rotation2 = Quaternion.Euler(-40F, 225F, 0F);  //left matrix
            rotation3 = Quaternion.Euler(-40F, -225F, 180F);  //right matrix
        }
        else if (index == 3 || index == 4)
        {
            Objective = frog2;
            rotation1 = Quaternion.Euler(-59.14F, 184.61F, 87.2F); //upper matrix
            rotation2 = Quaternion.Euler(-40F, 225F, 0F);  //left matrix
            rotation3 = Quaternion.Euler(-40F, -225F, 180F);  //right matrix
        }
        else if (index == 5 || index == 6)
        {
            Objective = frog3;
            rotation1 = Quaternion.Euler(-59.14F, 184.61F, 87.2F); //upper matrix
            rotation2 = Quaternion.Euler(-40F, 225F, 0F);  //left matrix
            rotation3 = Quaternion.Euler(-40F, -225F, 180F);  //right matrix
        }
        else if (index == 7 || index == 8 || index == 9) 
        {
            Objective = shark;
            rotation1 = Quaternion.Euler(-85.5811F, -0.2897F, 0F); //upper matrix
            rotation2 = Quaternion.Euler(-47.9599F, -115.7321F, 84.2607F);  //left matrix
            rotation3 = Quaternion.Euler(-47.3866F, -231.0016F, -108.7946F);  //right matrix
        }
        else if (index == 10)
        {
            Objective = elephant;
            rotation1 = Quaternion.Euler(6.7345F, 172.7119F, 90F); //upper matrix
            rotation2 = Quaternion.Euler(5.054F, 131.2116F, 49.058F);  //left matrix
            rotation3 = Quaternion.Euler(4.802F, 217.3609F, 141.2945F);  //right matrix
        }

        if (SelectFeaturesByLevel.level == "easy" || SelectFeaturesByLevel.level == "medium" || (!ValuesMainMenuScene.isADefaultLevel && ValuesMainMenuScene.lowerSceneLevel))
        {
            AppearWhenTheSceneIsDown();
        }
        else
        {
            AppearWhenTheSceneIsUp();
        }
    }

    void AppearWhenTheSceneIsDown()  // Here there is a Matrix (Up)
    {
        newPosition.x = (Random.Range(0, 4) * 2F) - 3F;    // A Matrix 4x4 of positions (Global positions):
        newPosition.z = (Random.Range(0, 4) * 1.8F) + 1.5F;     //    Range of x values = [-3.5; 5  ]
        newPosition.y = -0.15F;                                //    Range of y values = [ 7.6; 0.3]

        Instantiate(Objective, newPosition, rotation1);
    }

    void AppearWhenTheSceneIsUp()  // Here there are 3 Matrix (Up - Left - right)
    {
        int index = (int)Random.Range(0, 11);   // First, choose a matrix from 3

        switch ((int)index/3)  // Defines the 3 matrixes, there are 4 options because by this way, we can give more probabilities
        {                      // to the upper matrix

            case 0:  // Upper Matrix

                newPosition.x = (Random.Range(0, 4) * 2F) - 3F;    // A Matrix 4x4 of positions (Global positions):
                newPosition.z = (Random.Range(0, 4) * 1.5F) + 1.5F;     //    Range of x values = [-3.5; 5  ]
                newPosition.y = 1.22F;                                //    Range of y values = [ 7.6; 0.3]

                Instantiate(Objective, newPosition, rotation1);
                break;

            case 1:  // Left Matrix

                if (index % 3 == 0)
                {
                    newPosition.x = -4.1F;
                    newPosition.z = 2.8F;
                }

                if (index % 3 == 1)
                {
                    newPosition.x = -2.8F;
                    newPosition.z = 1.1F;
                }

                if (index % 3 == 2)
                {
                    newPosition.x = -1.0F;
                    newPosition.z = -0.7F;
                }

                newPosition.y = -0.2F;
                Instantiate(Objective, newPosition, rotation2);
                break;

            case 2:  // Right Matrix

                if (index % 3 == 0)
                {
                    newPosition.x = 5F;
                    newPosition.z = 2.4F;
                }

                if (index % 3 == 1)
                {
                    newPosition.x = 3.1F;
                    newPosition.z = 0.5F;
                }

                if (index % 3 == 2)
                {
                    newPosition.x = 1.4F;
                    newPosition.z = -1F;
                }

                newPosition.y = -0.2F;
                Instantiate(Objective, newPosition, rotation3);
                break;

            case 3:  // Upper Matrix Again. This case is the same that case 0, it's just for give more probabilities

                newPosition.x = (Random.Range(0, 4) * 2F) - 3F;    // A Matrix 4x4 of positions (Global positions):
                newPosition.z = (Random.Range(0, 4) * 1.5F) + 1.5F;     //    Range of x values = [-3.5; 5  ]
                newPosition.y = 1.22F;                                //    Range of y values = [ 7.6; 0.3]

                Instantiate(Objective, newPosition, rotation1);
                break;

        }
    }
}
