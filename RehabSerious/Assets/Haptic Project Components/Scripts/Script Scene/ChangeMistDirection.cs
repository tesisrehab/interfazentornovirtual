﻿using UnityEngine;
using System.Collections;

public class ChangeMistDirection : MonoBehaviour {

    public GameObject Mist;

	// Use this for initialization
	void Start () {
        Invoke("ChangeDirection", 15F + 3 * Random.Range(-1, 1));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void ChangeDirection()
    {
        Invoke("ChangeDirection", 15F + 3 * Random.Range(-1, 1));
        Mist.transform.eulerAngles = new Vector3(Mist.transform.eulerAngles.x, Mist.transform.eulerAngles.y + Random.Range(-2, 2), Mist.transform.eulerAngles.z);
    }
}
