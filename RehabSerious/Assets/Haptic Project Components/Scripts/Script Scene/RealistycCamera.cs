﻿using UnityEngine;
using System.Collections;

public class RealistycCamera : MonoBehaviour {

    Vector3 originalRotation = new Vector3();

	// Use this for initialization
	void Start () {
        InvokeRepeating("ChangeAngleCamera", 3, 0.02F);
        originalRotation = transform.rotation.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void ChangeAngleCamera()
    {
        float index = Random.Range(0, 100);
        if(index%6 == 0) transform.rotation = Quaternion.Euler(originalRotation + new Vector3(Random.Range(0, 0.2F), Random.Range(0, 0.2F), Random.Range(0, 0.2F)));
        else transform.rotation = Quaternion.Euler(originalRotation + new Vector3(Random.Range(0, 0.05F), Random.Range(0, 0.05F), Random.Range(0, 0.05F)));
    }
}
