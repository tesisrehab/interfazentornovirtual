﻿using UnityEngine;
using System.Collections;

public class ChangeToPlay : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void ChangeToPlayScene()
    {
        WaitAMoment();
        int index = Random.Range(0, 100) % 3;

        if (index == 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
        }
        else if (index == 1) 
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainSceneRain");
        }
        else if (index == 2)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainSceneMist");
        }
    }

    IEnumerator WaitAMoment()
    {
        float fadeTime = GameObject.Find("Dummy").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
    }
}
