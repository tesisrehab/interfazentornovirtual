﻿using UnityEngine;
using System.Collections;

namespace DigitalRuby.RainMaker
{
    public class StartRain : MonoBehaviour
    {
        public RainScript RainScript;

        // Use this for initialization
        void Start()
        {
            RainScript.RainIntensity = Random.Range(0.1F, 1);
            InvokeRepeating("ChangeRainy", 15F, 8F);
        }

        // Update is called once per frame
        void Update()
        {

        }

        void ChangeRainy()
        {
            RainScript.RainIntensity = Random.Range(0.1F, 1);
        }
    }
}

