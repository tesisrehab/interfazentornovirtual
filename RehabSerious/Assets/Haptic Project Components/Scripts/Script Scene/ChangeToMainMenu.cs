﻿using UnityEngine;
using System.Collections;

public class ChangeToMainMenu : MonoBehaviour {

    public Camera mainCamera;
    public Camera configurationCamera;

    public Canvas canvasMainCamera;
    public Canvas canvasConfigurationCamera;

    public UnityEngine.UI.Dropdown listOfLevelsDropdown;

    public UnityEngine.UI.Toggle defaultLevelsToogle;
    public UnityEngine.UI.Toggle automaticAxisForcesToogle;

    public UnityEngine.UI.Slider vibrationSlider;
    public UnityEngine.UI.Slider viscositySlider;
    public UnityEngine.UI.Slider forceSlider;

    public UnityEngine.UI.Toggle upperSceneLevelToggle;
    public UnityEngine.UI.Toggle lowerSceneLevelToggle;

    public UnityEngine.UI.Slider appearVelocitySlider;
    public UnityEngine.UI.Slider disappearVelocitySlider;
    public UnityEngine.UI.Slider timeSlider;

    
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void JumpToMainMenuFromOptions()
    {
        SaveValues();

        mainCamera.enabled = true;
        canvasMainCamera.enabled = true;

        configurationCamera.enabled = false;
        canvasConfigurationCamera.enabled = false;
    }

    void SaveValues()
    {
        ValuesMainMenuScene.isADefaultLevel = !defaultLevelsToogle.isOn;
        ValuesMainMenuScene.isRandomAxis = automaticAxisForcesToogle.isOn;
        ValuesMainMenuScene.vibrationChoosen = vibrationSlider.normalizedValue;
        ValuesMainMenuScene.viscosityChoosen = viscositySlider.normalizedValue;
        ValuesMainMenuScene.constantForceChoosen = forceSlider.normalizedValue;

        switch (listOfLevelsDropdown.value)
        {
            case 0:
                ValuesMainMenuScene.levelChoosen = "easy";
                break;
            case 1:
                ValuesMainMenuScene.levelChoosen = "medium";
                break;  
            case 2:
                ValuesMainMenuScene.levelChoosen = "hard";
                break;
            case 3:
                ValuesMainMenuScene.levelChoosen = "veryHard";
                break;
            case 4:
                ValuesMainMenuScene.levelChoosen = "legend";
                break;
        }

        if (lowerSceneLevelToggle.isOn)
        {
            ValuesMainMenuScene.lowerSceneLevel = true;
        }
        else
        {
            ValuesMainMenuScene.lowerSceneLevel = false;
        }

        ValuesMainMenuScene.velocityAppear = appearVelocitySlider.value;
        ValuesMainMenuScene.velocityDisappear = disappearVelocitySlider.value;
        ValuesMainMenuScene.minutesGame = timeSlider.value;
    }
}
