﻿using UnityEngine;
using System.Collections;

public class ValuesMainMenuScene : MonoBehaviour {

    public static string levelChoosen = "easy";

    public static bool isADefaultLevel = true;
    public static bool isRandomAxis = false;

    public static float viscosityChoosen = 0F;
    public static float vibrationChoosen = 0F;
    public static float constantForceChoosen = 0F;

    public static bool lowerSceneLevel = true;

    public static float velocityAppear = 3F;
    public static float velocityDisappear = 6.5F;
    public static float minutesGame = 1;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
}
