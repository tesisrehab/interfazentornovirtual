﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PointsCounter : MonoBehaviour {

    public static int goodPoints;    // HideInInspector is a comand for make that this variable be invisible in 
    public static int mediumPoints;  // the inspector
    public static int badPoints;
    public static int totalObjectives;
    public static int notHittedObjectves;
    public static int awards;

    private static List<float> timesThatFrogIsLive = new List<float>();

    private bool showPoint = false;
    

    // Use this for initialization
    void Start () {
        goodPoints = 0;
        mediumPoints = 0;
        badPoints = 0;
        totalObjectives = 0;
	}
	
	// Update is called once per frame
	void Update () {

    }

    public static void IncreaseTotalObjectives()
    {
        totalObjectives++;
    }

    public static void IncreaseGoodPoints()
    {
        goodPoints++;
    }

    public static void IncreaseMediumPoints()
    {
        mediumPoints++;
    }

    public static void IncreaseBadPoints()
    {
        badPoints++;
    }

    public static int GetNotHittedObjectives()
    {
        return (totalObjectives - (goodPoints + mediumPoints + badPoints));
    }

    public static void SaveNewTime(float time)
    {
        timesThatFrogIsLive.Add(time);
    }

    public static float GetAverageOfTimes()
    {
        if (Reset.isReset)
        {
            float acumulator = 0;

            foreach (float time in timesThatFrogIsLive)
            {
                acumulator = acumulator + time;
            }

            return acumulator / timesThatFrogIsLive.Count;
        }
        else
        {
            return 0;
        }
    }
}
