﻿using UnityEngine;
using System.Collections;

public class ChangeToConfiguration : MonoBehaviour {

    public Camera mainCamera;
    public Camera configurationCamera;
    public Canvas canvasMainCamera;
    public Canvas canvasConfigurationCamera;

	// Use this for initialization
	void Start () {
        mainCamera.enabled = true;
        canvasMainCamera.enabled = true;

        configurationCamera.enabled = false;
        canvasConfigurationCamera.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void JumpToSetupVideoGame()
    {
        mainCamera.enabled = false;
        canvasMainCamera.enabled = false;

        configurationCamera.enabled = true;
        canvasConfigurationCamera.enabled = true;
    }
}
