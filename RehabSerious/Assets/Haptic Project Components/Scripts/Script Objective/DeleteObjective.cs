﻿using UnityEngine;
using System.Collections;

public class DeleteObjective : MonoBehaviour {

    bool flag = false;  //Avoid two Collisions with the same crash

    public static float velocityDisappear = 6.5F;

    private float timeBaseAlive;
    private float timeAlive;
    
    public AudioSource impactFruit;

    void Awake()
    {
        velocityDisappear = ValuesMainMenuScene.velocityDisappear;
    }

    /* Use this for initialization */
    void Start () {
        timeBaseAlive = Time.time;
        PointsCounter.IncreaseTotalObjectives() ;
        flag = false;
	}
	
	/* Update is called once per frame */
	void Update () {
        DestroyObject(gameObject, velocityDisappear);
    }

    void OnCollisionEnter(Collision objectCollisioner)
    {
        if (objectCollisioner.relativeVelocity.sqrMagnitude < 250 && flag == false) 
        {
            impactFruit.Play();
            Categorization(objectCollisioner);  // Defines if the hit was good, medium or bad
            DestroyObject(gameObject, 0.265F);
            flag = true;                                  
        }
    }

    void Categorization(Collision objectCollisioner)
    {
        float goodLimit = 0.25F;      // Tolerance from measure of goodPoint   (middle of a square)
        float mediumLimit = 0.5F;     // Tolerance from measure of mediumPoint (middle of a square)

        print(SelectFeaturesByLevel.level);
        if (gameObject.transform.position.y < 1F && (SelectFeaturesByLevel.level != "easy" && SelectFeaturesByLevel.level != "medium"))  // Asses if the Object is in the matrix 2 or 3 for use another type of catergorization
        {
            goodLimit = goodLimit + 0.05F;      // In that part, we increase the tolerance because the objectives on this position
            mediumLimit = mediumLimit + 0.05F;  // are more hard
            if (objectCollisioner.transform.position.y < (gameObject.transform.position.y + goodLimit) && objectCollisioner.transform.position.y > (gameObject.transform.position.y - goodLimit) && objectCollisioner.transform.position.z < (gameObject.transform.position.z + goodLimit) && objectCollisioner.transform.position.z > (gameObject.transform.position.z - goodLimit))
            {
                PointsCounter.IncreaseGoodPoints();
            }
            else
            {
                if (objectCollisioner.transform.position.y < (gameObject.transform.position.y + mediumLimit) && objectCollisioner.transform.position.y > (gameObject.transform.position.y - mediumLimit) && objectCollisioner.transform.position.z < (gameObject.transform.position.z + mediumLimit) && objectCollisioner.transform.position.z > (gameObject.transform.position.z - mediumLimit))
                {
                    PointsCounter.IncreaseMediumPoints();
                }
                else
                {
                    PointsCounter.IncreaseBadPoints();
                }
            }
        }
        else
        {
            if (objectCollisioner.transform.position.x < (gameObject.transform.position.x + goodLimit) && objectCollisioner.transform.position.x > (gameObject.transform.position.x - goodLimit) && objectCollisioner.transform.position.z < (gameObject.transform.position.z + goodLimit) && objectCollisioner.transform.position.z > (gameObject.transform.position.z - goodLimit))
            {
                PointsCounter.IncreaseGoodPoints();
            }
            else
            {
                if (objectCollisioner.transform.position.x < (gameObject.transform.position.x + mediumLimit) && objectCollisioner.transform.position.x > (gameObject.transform.position.x - mediumLimit) && objectCollisioner.transform.position.z < (gameObject.transform.position.z + mediumLimit) && objectCollisioner.transform.position.z > (gameObject.transform.position.z - mediumLimit))
                {
                    PointsCounter.IncreaseMediumPoints();
                }
                else
                {
                    PointsCounter.IncreaseBadPoints();
                }
            }
        }        
    }

    void OnDisable()
    {
        timeAlive = Time.time - timeBaseAlive;   // This is the time that the frog has been alive

        PointsCounter.SaveNewTime(timeAlive);  // So, we save that value
    }
}
