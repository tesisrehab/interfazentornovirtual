﻿using UnityEngine;
using System.Collections;

public class ViscosityEffect : MonoBehaviour {

	public string Type;
	public int effect_index;
	public float gain;
	public float magnitude;
	public float duration;
	public float frequency;
	public float[] positionEffect =  new float[3];
	public float[] directionEffect = new float[3];

	// Use this for initialization
	void Start () {
		Type = "viscous";
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (SelectFeaturesByLevel.viscosityActivated == true)
        {
            Type = "viscous";
            
            gain = SelectFeaturesByLevel.gainViscositySelected;
            magnitude = SelectFeaturesByLevel.magnitudeViscositySelected;
        }
        else
        {
            Type = null;
            gain = 0F;
            magnitude = 0F;
            directionEffect[0] = 0;
            directionEffect[1] = 0;
            directionEffect[2] = 0;
        }
    }
}
