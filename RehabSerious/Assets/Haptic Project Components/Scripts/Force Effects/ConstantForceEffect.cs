using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

public class ConstantForceEffect : MonoBehaviour {

	public string Type;
	public int effect_index;
	public float gain;
	public float magnitude;
	public float duration;
	public float frequency;
	public float[] positionEffect =  new float[3];
	public float[] directionEffect = new float[3];
	
	
	// Use this for initialization
	void Start () {
		Type = "constant";
	}
	
	// Update is called once per frame
	void Update () {
        if (SelectFeaturesByLevel.forcesActivated == true)
        {
            Type = "constant";

            effect_index = 0;
            gain = SelectFeaturesByLevel.gainForceSelected;
            magnitude = SelectFeaturesByLevel.magnitudeForceSelected;
            frequency = SelectFeaturesByLevel.frecuenceForceSelected;
            directionEffect = SelectFeaturesByLevel.directionForceSelected;
        }
        else
        {
            Type = null;
            gain = 0F;
            magnitude = 0F;
            directionEffect[0] = 0;
            directionEffect[1] = 0;
            directionEffect[2] = 0;
        }
	}
}
